#!/bin/bash

# sudo apt-get install -y unity virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
# sudo VBoxClient-all
# 
# sudo vim /etc/X11/Xwrapper.config and edit it to allowed_users=anybody


# Install docker
sudo apt-get update
sudo apt-get install linux-image-generic-lts-trusty
sudo apt-get install wget
wget -qO- https://get.docker.com/ | sh
usermod -aG docker vagrant

# Clean
sudo apt-get autoremove -y