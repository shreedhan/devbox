# README #

Setting up a devbox using vagrant and docker

### What is this repository for? ###

* Setting up a portable dev box
## Your title here... ##
### How do I get set up? ###
* Install vagrant
* Clone this repository inside `~/dev/vagrant/` without modifying name
* `cd ~/dev/vagrant/devbox`
* Run vagrant using `vagrant up` command
* SSH into the vagrant box using `vagrant ssh` command
* --> `docker build -t devserver --rm=true /data/devbox/servers/development/`
* Run the docker image as follows
* --> `docker run --name web -v /data:/data -p 8080:8080 -d devserver`
* This will run Apache Tomcat on port 8080. Port 8080 from docker container is forwarded to Vagrant's port 8080. Vagrant's port 8080 is forwarded to host's port 38080.
* You should be able to access localhost:38080 from your main host, localhost:8080 from your vagrant host and docker container

## What do you get ##
* Apache tomcat at `http://[host]:38080`. The default username and password is `developer`, `developer`
* Jenkins at http://[host]:8080/jenkins
* GitBucket at http://[host]:8080/gitbucket
* Maven
* Gradle

### Who do I talk to? ###

* Shreedhan Shrestha - shreedhan.s@gmail.com